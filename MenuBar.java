/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate MenuBar objects.
 * <p>
 * Menu bars appear at the top and bottom of the gameboard. They are used to
 * hold game statistics boxes.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class MenuBar extends MyWorldActor {
    
    private String pic;

    /**
     * Primary constructor for objects of class MenuBar.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param pic the name of the menu bar logo to be displaye, if any
     * @since 1.0
     */
    public MenuBar(String pic) {
        this.pic = pic;
    }    
    
    public MenuBar() {}
    
    /**
     * Builds the MenuBar image and adds it to the World.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void buildImage() {
        GreenfootImage bar = new GreenfootImage(realWorld.getWidth()*2, 24);
        bar.setColor(new Color(255, 148, 0));
        bar.fillRect(0,0,realWorld.getWidth()*2, bar.getHeight());
        try {
            if(pic != null) {
                GreenfootImage image = new GreenfootImage(pic);
                image.scale(88,24);
                bar.drawImage(image,(realWorld.getWidth()/2+realWorld.getWidth()-44),(bar.getHeight()-image.getHeight())/2);
            }
            setImage(bar);
        } catch (Exception e) {
            realWorld.fileMissingErrorHandler(e,true);
        }
    }
}
