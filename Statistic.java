/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class provides the makeStatisticBox method to all inheriting actor objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Statistic extends TimedActor {
    
    private GreenfootImage value;
    private GreenfootImage label;
    private GreenfootImage image;
    protected GreenfootImage fullImage;
    
    /**
     * Builds a statistic box image to use for displaying a statistic.
     * 
     * @author (Doug Loomer) 
     * @version (1.0)
     * @param labelText the label to appear in the statistic box
     * @param valueText the statistic value to be displayed
     * @since (1.0)
     */
    protected void makeStatisticBox(String labelText, String valueText) {
        // Create a maxWidth for the value to be displayed
        GreenfootImage minValueWidth = new GreenfootImage("0",11,Color.BLACK,Color.BLACK);
        int maxValueWidth = minValueWidth.getWidth()+34;
        
        // Instantiate basic images to be combined and displayed
        value = new GreenfootImage(valueText+" ",11,Color.WHITE,new Color(255,148,0));
        label = new GreenfootImage(labelText+" ",12,Color.BLACK,new Color(193,196,199));
        image = new GreenfootImage(maxValueWidth,value.getHeight());
        fullImage = new GreenfootImage(image.getWidth()+label.getWidth()+20,image.getHeight()+4);
        
        // Draw the image that will hold the value to be displayed
        image.setColor(new Color(255,148,0));
        image.fill();
        image.drawRect(0,0,image.getWidth(),image.getHeight());
        image.drawImage(value,(image.getWidth()-value.getWidth())/2,(image.getHeight()-value.getHeight()+2)/2);
        
        // Draw the image that will hold the label and the value to be displayed
        fullImage.setColor(new Color(193,196,199));
        fullImage.fill();
        fullImage.drawRect(0,0,fullImage.getWidth()-1,fullImage.getHeight()-1);
        fullImage.drawImage(image,(fullImage.getWidth()-image.getWidth()-7),(fullImage.getHeight()-image.getHeight())/2);
        fullImage.drawImage(label,5,(fullImage.getHeight()-image.getHeight())/2);
        
        // Display the full label/value image
        setImage(fullImage);
    }
    
    /**
     * Gets the width of the statistic box.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the width of the statistics box
     * @since 1.0
     */
    protected int getStatisticBoxWidth() {
        return fullImage.getWidth();
    }
}
