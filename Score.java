/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Score objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Score extends Statistic {
    
    private int score;
    
    /**
     * Primary constructor for objects of class Score.
     *
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    public Score() {
        score = 0;
        makeStatisticBox("Score",""+score);
    }
    
    /**
     * Adds 10 to the value of the score variable and updates the Score statistics box. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void updateScore() {
        score += 10;
        makeStatisticBox("Score",""+score);
    }
    
    /**
     * Gets the value of the score variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the score variable
     * @since 1.0
     */
    protected int getScore() {
        return this.score;
    }
}
