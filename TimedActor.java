/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class provides the controlActCount method to all inheriting actor
 * objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class TimedActor extends MyWorldActor {
    
    protected int actCount = 1;
    
    public TimedActor() {}
    
    /**
     * Tracks how many times the act() method has been called by Greenfoot.
     * <p>
     * The actCountMax parameter sets a maximum limit to the value of actCount,
     * and actCount is incremented each time controlActCount() is called. The
     * value of actCount can be used by objects to control how often a method
     * called by that objects act() method is executed, thereby controlling 
     * the speed of aspects of object behavior.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param actCountMax the maximum value of actCount before actCount is reset
     * @since 1.0
     */
    protected void controlActCount(int actCountMax){
        actCount++;
        if(actCount>actCountMax){
            actCount = 1;
        }
    }
}
