/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This abstract class is used to provide inheriting actors with a common checkCollision()
 * method, and make generic actions on all inheriting actors possible. 
 * <p>
 * For example, because this parent
 * Meteor class exist, the code {@code removeObjects(getObjects(Meteor.class));} removes all
 * Meteor objects and objects of classes extending Meteor.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public abstract class Meteor extends TimedActor
{   
    protected MyWorld realWorld;
    
     /**
     * Primary constructor for objects of class Meteor.
     * <p>
     * Explosions come in two sizes - large and small.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param realWorld a reference to the MyWorld gameboard object
     * @since 1.0
     */
    public Meteor(MyWorld realWorld) {
        this.realWorld = realWorld;
    }

    /**
     * Handles Meteor behavior after touching Torpedo or Ship objects.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param explosionSize how large a Meteor's explosion will be
     * @param type the name of the type of Meteor to be affected
     * @since 1.0
     */
    protected void checkCollision(int explosionSize,String type) {
        if(isTouching(Torpedo.class) || isTouching(Ship.class)) {
            Explosion myExplosion = new Explosion(explosionSize);
            realWorld.addObject(myExplosion,getX(),getY());
            if(isTouching(Torpedo.class)) {
                realWorld.getScore().updateScore();
                realWorld.removeObjects(getObjectsInRange(100,Torpedo.class));
                if(type.equals("medium")) {
                    realWorld.createMedMeteor(getX(),getY(),realWorld.getNumMedium());
                }
                if(type.equals("small")) {
                    realWorld.createSmallMeteor(getX(),getY(),realWorld.getNumSmall());
                }
                realWorld.removeObject(this);
            } else {
                if(isTouching(Ship.class)) {
                    if(this instanceof BigMeteor) {
                        realWorld.getScore().updateScore();
                        realWorld.getHealth().adjustHealth(-10);
                        realWorld.removeObjects(realWorld.getObjects(Torpedo.class));
                        realWorld.hideShip();
                    }
                    if(this instanceof MediumMeteor) {
                        realWorld.getScore().updateScore();
                        realWorld.getHealth().adjustHealth(-5);
                        realWorld.createSmallMeteor(getX(),getY(),3);
                        realWorld.getShip().setThrust(1);
                        realWorld.removeObject(this);
                    }
                    if(this instanceof SmallMeteor) {
                        realWorld.getHealth().adjustHealth(-2);
                        realWorld.getScore().updateScore();
                        realWorld.removeObject(this);
                    }
                }
            }
        }
    }
    
    /**
     * An abstract method to require inheriting class to implement a seSpeed() method.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected abstract void setSpeed();
    
    /**
     * An abstract method to require inheriting class to implement a stayOnScreen() method.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected abstract void stayOnScreen();
}
