/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;  

/**
 * This class is used to instantiate Button objects.
 * <p>
 * Buttons are used to let the user select game difficulty levels and
 * play the game again.
 *  
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Button extends MyWorldActor {
    
    private boolean realWorldIsSet = false;
    private String buttonName;
    private String buttonTarget;
    private GreenfootImage buttonImage;
    private GreenfootSound click;
    
    /**
     * Primary constructor for objects of class Button.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param buttonImage the image file to be used to display the button
     * @param buttonName the name of the button
     * @param buttonTarget the name of the message the button will activate
     * @since 1.0
     */
    public Button(GreenfootImage buttonImage,String buttonName,String buttonTarget) {
        this.buttonImage = buttonImage;
        this.buttonTarget = buttonTarget;
        this.buttonName = buttonName;
        setImage(buttonImage);
        scaleImage(buttonImage);
    }
    
    public Button() {}
    
    /**
     * Sets the value of the inherited realWorld reference variable to an 
     * object of type MyWorld. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void setRealWorld() {
        if(realWorldIsSet==false) {
            super.setRealWorld();
            realWorldIsSet = true;
        }
    }
    
    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        setRealWorld();
        onClick();
    }
    
    /**
     * Handles onclick behavior of button object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void onClick() {
        if (Greenfoot.mousePressed(this)) {
            try {
                click = new GreenfootSound("sounds/click.wav");
            } catch (Throwable e) {
                realWorld.fileMissingErrorHandler(e,true);
            }
            try {
                click.play();
            } catch (Throwable e) {
                realWorld.fileMissingErrorHandler(e,true);
            }
            MyWorld myWorld = (MyWorld)getWorld();
            if(buttonName.equals("easy") || buttonName.equals("hard") || buttonName.equals("nuts")) {
                myWorld.setDifficulty(buttonName);
                myWorld.setGameLevelVariables();
                myWorld.setNextLevel();
                return;
            }
            if(this.buttonName.equals("playAgain")) {               
                Greenfoot.setWorld(new MyWorld());
                return;
            }
            myWorld.displayIntroMessage(buttonTarget);
        }
    }
    
    /**
     * Scales the button image depending upon which type of button is to be
     * displayed.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param thisImage a GreenfootImage object for the button
     * @since 1.0
     */
    private void scaleImage(GreenfootImage thisImage) {
        if(buttonName.equals("easy") || buttonName.equals("hard") || buttonName.equals("nuts")) {
            thisImage.scale(40,40);
        } else if(buttonName.equals("playAgain")) {
            thisImage.scale(120,80);
        } else {
            thisImage.scale(20,20);
        }
    }
}
