/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Message objects.
 * <p>
 * Messages are used to provide basic game play instructions, describe 
 * game difficulty levels, and provide game flow information to users.
 *  
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Message extends MyWorldActor {
    
    private GreenfootImage messageImage;
    private String messageName;
    private boolean messageBig;
    private String priorMessageName;
    private String nextMessageName;
    private boolean buttonsSet = false;
    
    /**
     * Primary constructor for objects of class Message.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param realWorld a reference to the MyWorld gameboard object
     * @param messageImage a reference to the Message image to be used
     * @param messageName the name of the message
     * @param messageBig whether this is to be a large scaled message
     * @param priorMessageName the name of the prior message displayed, or null
     * @param nextMessageName the name of the next message to be displayed, or null
     * @since 1.0
     */
    public Message(MyWorld realWorld,GreenfootImage messageImage,String messageName,boolean messageBig,String priorMessageName,String nextMessageName) {
        this.messageName = messageName;
        this.messageBig = messageBig;
        this.priorMessageName = priorMessageName;
        this.nextMessageName = nextMessageName;
        this.messageImage = messageImage;
        sizeImage();
        setImage(messageImage);
    }
    
    public Message() {}

    /**
     * Gets the value of the messageName variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the messageName variable
     * @since 1.0
     */
    protected String getMessageName() {
        return messageName;
    }
    
    /**
     * Scales the Message image depending upon which type of message is to be
     * displayed.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void sizeImage() {
        if(messageName.equals("youWinMess") || messageName.equals("youLoseMess")) {
            messageImage.scale(807,513);
            return;
        } else if(messageBig) {
            messageImage.scale(404,257);
        } else {
            messageImage.scale(249,147);
        }
    }
    
    /**
     * Handles instantiating and displaying the appropriate button(s) to be displayed with the message..
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void setButtons() {
        if(buttonsSet==false) {
            if(priorMessageName!=null) {
                Button leftButton = new Button(realWorld.getLeftArrow(),"leftArrow",priorMessageName);
                realWorld.addObject(leftButton,getX()-100,getY()+50);
            }
            
            if(nextMessageName!=null) {
                Button rightButton = new Button(realWorld.getRightArrow(),"rightArrow",nextMessageName);
                realWorld.addObject(rightButton,getX()+100,getY()+50);
            }

            if(messageName.equals("intro5Mess")) {
                Button easyButton = new Button(realWorld.getEasy(),"easy",null);
                realWorld.addObject(easyButton,getX()-50,getY()+30);
                Button hardButton = new Button(realWorld.getHard(),"hard",null);
                realWorld.addObject(hardButton,getX()+0,getY()+30);
                Button nutsButton = new Button(realWorld.getNuts(),"nuts",null);
                realWorld.addObject(nutsButton,getX()+50,getY()+30);
            }
            buttonsSet = true;
        }
    }
    
    /**
     * Builds and instantiates a GreenfootImage object to be displayed with
     * the You Win message. 
     * <p>
     * The image generated includes all final game scores.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void addScores() {
        GreenfootImage score = new GreenfootImage(realWorld.getScore().getScore()+"", 34, Color.BLACK, new Color(255,212,153));
        messageImage.drawImage(score, messageImage.getWidth()/2+130, messageImage.getHeight()/2-108);
        GreenfootImage lives = new GreenfootImage(realWorld.getLives().getLives()+"", 34, Color.BLACK, new Color(255,212,153));
        messageImage.drawImage(lives, messageImage.getWidth()/2+130, messageImage.getHeight()/2-65);
        GreenfootImage totalTime = new GreenfootImage(realWorld.getTotalTime()+"", 34, Color.BLACK, new Color(255,212,153));
        messageImage.drawImage(totalTime, messageImage.getWidth()/2+130, messageImage.getHeight()/2-23);
        GreenfootImage totalTorpedos = new GreenfootImage(realWorld.getTorpedos().getTorpedos()+"", 34, Color.BLACK, new Color(255,212,153));
        messageImage.drawImage(totalTorpedos, messageImage.getWidth()/2+130, messageImage.getHeight()/2+18);
        GreenfootImage finalScore = new GreenfootImage(realWorld.getFinalScore()+"", 40, Color.BLACK, new Color(255,212,153));
        messageImage.drawImage(finalScore, messageImage.getWidth()/2+130, messageImage.getHeight()/2+69);
    }
    
    /**
     * Gets the value of the buttonsSet variable.
     * <p>
     * The buttonsSet variable tracks whether the setButtons method of a 
     * Message object has been set.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the buttonsSet variable
     * @since 1.0
     */
    protected void setButtonsSet(boolean state) {
        buttonsSet = state;
    }
}
