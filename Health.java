/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Health objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Health extends Statistic {
    
    private int health;

    /**
     * Primary constructor for objects of class Health.
     *
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    public Health() {
        health = 1;
        updateHealth();
    }

    /**
     * Subtracts 1 from the value of the health variable, makes sure it does not become
     * a negative number, and updates the Health statistics box. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void updateHealth() {
        health--;
        if(health<0) {
            health = 0;
        }
        makeStatisticBox("Health",""+health);
    }

    /**
     * Sets the value of the health variable to the value passed and updates the health
     * statistic box.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param health the value to which the health variable should be set
     * @since 1.0
     */
    protected void setHealth(int health) {
        this.health = health;
        makeStatisticBox("Health",""+health);
    }

    /**
     * Gets the value of the health variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the health variable
     * @since 1.0
     */
    protected int getHealth() {
        return health;
    }

    /**
     * Adjusts the value of the health variable by the value passed, makes sure it does not
     * become a negative number, and updates the Health statistics box..
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param health the value to adjust the health variable by
     * @since 1.0
     */
    protected void adjustHealth(int health) {
        this.health += health;
        if((this.health)<1) {
            this.health = 0;
        }
        makeStatisticBox("Health",""+this.health);
    }
}
