/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*; 

/**
 * This class provides getter and setter methods for accessing the gameboard 
 * as a MyWorld object.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class MyWorldActor extends Actor {

    protected MyWorld realWorld;

    /**
     * Sets the value of the realWorld reference variable to an object of
     * type MyWorld. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void setRealWorld() {
        realWorld = (MyWorld)getWorld();
    }

    /**
     * Gets a MyWorld type reference to the game World. This enables
     * inheriting actor classes to call game board related methods without
     * casting each time.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the game board cast as a MyWorld object
     * @since 1.0
     */
    protected MyWorld getRealWorld() {
        return (MyWorld)getWorld();
    }
}
