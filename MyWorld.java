/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate the gameboard and populate it with game 
 * objects. It contains game level state variables, as well as methods to 
 * access game objects, to contol game flow and object interactions, and
 * to handle errors and exceptions.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class MyWorld extends World {

    private Message[] gameMessages;
    private Message currentMessage = null;
    private GreenfootImage[] mediumMeteorTypes;
    private GreenfootImage[] smallMeteorTypes;
    private String difficulty;
    private Health health;
    private Lives lives;
    private Torpedos torpedos;
    private Level level;
    private Timer timer;
    private Ship ship;
    private Score score;
    private int totalTime = 0;
    private int level2Interval;
    private int level3Interval;
    private int speedBig;
    private int speedMedium;
    private int speedSmall;
    private int numMedium;
    private int numSmall;
    private boolean bigMeteorSpawned = false;
    private int finalScore;
    private boolean gameOver = false;
    private boolean smallMeteorSpawned = false;
    private int spawnTime;
    private boolean realWorldIsRunning;
    private GreenfootImage introduction1;
    private GreenfootImage introduction2;
    private GreenfootImage introduction3;
    private GreenfootImage introduction4;
    private GreenfootImage introduction5;
    private GreenfootImage congratulations1;
    private GreenfootImage congratulations2;
    private GreenfootImage tryLevelAgain;
    private GreenfootImage youLose;
    private GreenfootImage youWin;
    private GreenfootImage easy;
    private GreenfootImage hard;
    private GreenfootImage nuts;
    private GreenfootImage playAgain;
    private GreenfootImage rightArrow;
    private GreenfootImage leftArrow;
    private GreenfootSound explosionSoundLocal;
    private static GreenfootSound spaceSound;

    /**
     * Primary constructor for objects of class MyWorld.
     * <p>
     * Note that it calls the {@link #prepareGameBoard()} 
     * helper method to instantiate gameboard objects.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    public MyWorld() {    
        super(900,600,1,false);
        Greenfoot.setSpeed(50);
        setPaintOrder(Statistic.class,MenuBar.class,Button.class,Message.class,Explosion.class,Ship.class,Torpedo.class,Meteor.class);
        prepareGameBoard();
    }

    /**
     * Secondary constructor for objects of class MyWorld.
     * <p>
     * It creates an empty world for displaying error and exception
     * messages handled by {@link #fileMissingErrorHandler(Throwable e,Boolean worldInstantiated)}
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param errorText error message to be displayed
     * @since 1.0
     */
    public MyWorld(String errorText) {
        super(900,600,1,false);
        realWorldIsRunning = false;
        showText("Hey... "+errorText,getWidth()/2,getHeight()/2);
        Greenfoot.stop();
    }

    /**
     * Helper method for primary class constructor.
     * <p>
     * It instantiates the initial world objects and adds them to the world.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0 
     */
    private void prepareGameBoard()
    {
        try {
            introduction1 = new GreenfootImage("messages/Introduction1.png");
            introduction2 = new GreenfootImage("messages/Introduction2.png");
            introduction3 = new GreenfootImage("messages/Introduction3.png");
            introduction4 = new GreenfootImage("messages/Introduction4.png");
            introduction5 = new GreenfootImage("messages/Introduction5.png");
            congratulations1 = new GreenfootImage("messages/Congratulations1.png");
            congratulations2 = new GreenfootImage("messages/Congratulations2.png");
            tryLevelAgain = new GreenfootImage("messages/TryLevelAgain.png");
            youLose = new GreenfootImage("messages/YouLose.png");
            youWin = new GreenfootImage("messages/YouWin.png");
            gameMessages = new Message[10];
            gameMessages[0] = new Message(this,introduction1,"intro1Mess",false,null,"intro2Mess");
            gameMessages[1] = new Message(this,introduction2,"intro2Mess",false,"intro1Mess","intro3Mess");
            gameMessages[2] = new Message(this,introduction3,"intro3Mess",false,"intro2Mess","intro4Mess");
            gameMessages[3] = new Message(this,introduction4,"intro4Mess",false,"intro3Mess","intro5Mess");
            gameMessages[4] = new Message(this,introduction5,"intro5Mess",false,"intro4Mess",null);
            gameMessages[5] = new Message(this,congratulations1,"congrat1Mess",false,null,null);
            gameMessages[6] = new Message(this,congratulations2,"congrat2Mess",false,null,null);
            gameMessages[7] = new Message(this,tryLevelAgain,"tryAgainMess",false,null,null);
            gameMessages[8] = new Message(this,youLose,"youLoseMess",false,null,null);
            gameMessages[9] = new Message(this,youWin,"youWinMess",true,null,null);
            mediumMeteorTypes = new GreenfootImage[8];
            mediumMeteorTypes[0] = new GreenfootImage("Medium1.png");
            mediumMeteorTypes[1] = new GreenfootImage("Medium2.png");
            mediumMeteorTypes[2] = new GreenfootImage("Medium3.png");
            mediumMeteorTypes[3] = new GreenfootImage("Medium4.png");
            mediumMeteorTypes[4] = new GreenfootImage("Medium1.png");
            mediumMeteorTypes[5] = new GreenfootImage("Medium2.png");
            mediumMeteorTypes[6] = new GreenfootImage("Medium3.png");
            mediumMeteorTypes[7] = new GreenfootImage("Medium4.png");
            smallMeteorTypes = new GreenfootImage[6];
            smallMeteorTypes[0] = new GreenfootImage("Small1.png");
            smallMeteorTypes[1] = new GreenfootImage("Small2.png");
            smallMeteorTypes[2] = new GreenfootImage("Small3.png");
            smallMeteorTypes[3] = new GreenfootImage("Small1.png");
            smallMeteorTypes[4] = new GreenfootImage("Small2.png");
            smallMeteorTypes[5] = new GreenfootImage("Small3.png");
            easy = new GreenfootImage("easy.png");
            hard = new GreenfootImage("hard.png");
            nuts = new GreenfootImage("nuts.png");
            leftArrow = new GreenfootImage("arrowLeft.png");
            rightArrow = new GreenfootImage("arrowRight.png");
            playAgain = new GreenfootImage("playAgain.png");
            explosionSoundLocal = new GreenfootSound("sounds/explosion.wav");
            spaceSound = new GreenfootSound("sounds/space.wav");
            MenuBar menubar1 = new MenuBar("saveEarthLogo.png");
            addObject(menubar1,0,menubar1.getImage().getHeight()/2+4);
            menubar1.setRealWorld();
            menubar1.buildImage();
            MenuBar menubar2 = new MenuBar(null);
            addObject(menubar2,0,getHeight()-menubar1.getImage().getHeight()/2);
            menubar2.setRealWorld();
            menubar2.buildImage();
            timer = new Timer();
            addObject(timer,46,getHeight()-menubar2.getImage().getHeight()/2);
            timer.makeStatisticBox("Time",""+timer.getTime());
            level = new Level();
            addObject(level,48,menubar1.getImage().getHeight()/2);
            torpedos = new Torpedos();
            addObject(torpedos,183,getHeight()-menubar2.getImage().getHeight()/2);
            lives = new Lives();
            addObject(lives,getWidth()-(lives.getStatisticBoxWidth()/2)-5,getHeight()-menubar2.getImage().getHeight()/2);
            health = new Health();
            addObject(health,getWidth()-(health.getStatisticBoxWidth()/2)-5-lives.getStatisticBoxWidth()-18,getHeight()-menubar2.getImage().getHeight()/2);
            score = new Score();
            addObject(score,getWidth()-(score.getStatisticBoxWidth()/2)-5,menubar1.getImage().getHeight()/2);
            ship = new Ship();
            addObject(ship,getWidth()/2,getHeight()-50);
            ship.setRealWorld();
            ship.setRocketImages();
            currentMessage = gameMessages[0];
            addObject(currentMessage,getWidth()/2,getHeight()/2);
            currentMessage.setRealWorld();
            currentMessage.setButtons();
            realWorldIsRunning = true;
        } catch (Throwable e) {
            fileMissingErrorHandler(e,false);
        }
    }
    
    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        if(realWorldIsRunning==true) {
            checkMeteorsRemaining();
            checkBigMeteorSpawnTime();
            checkHealthRemaining();
            checkLivesAndTorpedosRemaining();
            System.out.println(numberOfObjects());
        }
    }

    /**
     * Handles errors and exceptions.
     * <p>
     * This method is called by catch blocks in the program. It handles
     * errors and exceptions, generates a human readable message, and
     * displays that message in the MyWorld gameboard.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param e a throwable error or exception object
     * @param worldInstantiated whether the World was successfully created
     * @since 1.0
     */
    protected void fileMissingErrorHandler(Throwable e,Boolean worldInstantiated) {
        String errorText = "";
        if(e instanceof ExceptionInInitializerError) {
            errorText = ""+e.getCause();
            errorText = "The file \""+errorText.substring(errorText.indexOf("file:")+6)+"\" can't be loaded\nso the program is stopping.\nThe file is probably missing or corrupted.";
        }
        if(e instanceof NoClassDefFoundError) {
            errorText = ""+e.getCause();
            errorText = "An object of type \""+errorText.substring(errorText.indexOf("ize class")+10)+"\" can't be loaded\nso the program is stopping.\nA file that the program needs to create a Ship object\nis probably missing or corrupted.";
        }
        if(e instanceof IllegalArgumentException) {
            errorText = ""+e.getCause();
            errorText = "The file \""+errorText.substring(errorText.indexOf("file:")+6)+"\" can't be loaded\nso the program is stopping.\nThe file is probably missing or corrupted.";
        }
        if(e instanceof NullPointerException) {
            return;
        }
        if(worldInstantiated==true) {
            Greenfoot.setWorld(new MyWorld(errorText));
        } else {
            showText(errorText,getWidth()/2,getHeight()/2);
        }
    }

    /**
     * Loops background music when the program begins executing.
     * <p>
     * Overrides the World class started() method which is called 
     * automatically by Greenfoot each time the program begins executing.
     * The started() method of the World class does nothing. Here, it is
     * overridden to control starting the game's background music.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void started() {
        spaceSound.playLoop();
    }
    
    /**
     * Stops background music when the program stops executing.
     * <p>
     * Overrides the World class stopped() method which is called 
     * automatically by Greenfoot each time the program stops executing.
     * The stopped() method of the World class does nothing. Here, it is
     * overridden to control stopping the game's background music.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void stopped() {
        spaceSound.stop();
    }

    /**
     * Manages the display of the game's Introduction messages.
     * <p>
     * The method removes any messages and buttons currently displayed,
     * determines which message and button(s) is to be displayed, and
     * adds them to the World.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param buttonTarget the name of the right-most button to be displayed or null
     * @since 1.0
     */
    protected void displayIntroMessage(String buttonTarget) {
        int r=0;
        do {
            if(buttonTarget.equals(gameMessages[r].getMessageName())) {
                removeObjects(getObjects(Button.class));
                removeObject(currentMessage);
                currentMessage = gameMessages[r];
                currentMessage.setButtonsSet(false);
                addObject(currentMessage,getWidth()/2,getHeight()/2);
                currentMessage.setRealWorld();
                currentMessage.setButtons();
                break;
            }
            r++;
        } while(r<gameMessages.length);
    }

    /**
     * Manages the display of game progress messages.
     * <p>
     * The method determines which message and button(if any) is to be 
     * displayed (e.g. You Win, Try Again, Game Over),adds them to the World,.
     * clears other objects from the gameboard, and adjusts game level and 
     * timer values/execution where appropriate.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param target the name of the message to be displayed
     * @since 1.0
     */
    protected void displayProgressMessage(String target) {
        for(int r=0; r<gameMessages.length; r++) {
            if(target.equals(gameMessages[r].getMessageName())) {
                currentMessage = gameMessages[r];
                addObject(currentMessage,getWidth()/2,getHeight()/2);
                break;
            }
        }        
        if(!(target.equals("youWinMess")) && !(target.equals("youLoseMess"))) {
            hideShip();
            Greenfoot.delay(180);
            removeObjects(getObjects(Explosion.class));
            removeDebris();
            removeObjects(getObjects(Meteor.class));
            removeObject(this.currentMessage);
            if(target.equals("tryAgainMess")) {
                startLevel();
            } else {
                setNextLevel();
            }
        }
        if(target.equals("youWinMess")) {
            currentMessage = gameMessages[9];            
            addObject(currentMessage,getWidth()/2,getHeight()/2);
            currentMessage.setRealWorld();
            currentMessage.addScores();
            Button playAgainButton = new Button(playAgain,"playAgain",null);
            addObject(playAgainButton,getWidth()-150,getHeight()-120);
            removeObject(ship);
            timer.stopTimer();
            gameOver = true;
        }
        if(target.equals("youLoseMess")) {
            removeDebris();
            removeObjects(getObjects(Meteor.class));
            currentMessage = gameMessages[8];            
            addObject(currentMessage,getWidth()/2,getHeight()/2);
            level.setLevel(0);
            currentMessage.setRealWorld();
            Button playAgainButton = new Button(playAgain,"playAgain",null);
            addObject(playAgainButton,getWidth()-150,getHeight()-120);
            removeObject(ship);
            timer.stopTimer();
            gameOver = true;
        }
     }

    /**
     * Sets multiple game level variables.
     * <p>
     * This method sets the game difficulty level variables, the number of
     * lives the user will have, and sets the timer to 0. It is called
     * when the user selects a game difficulty level - easy, hard, or nuts.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void setGameLevelVariables() {
        setDifficultyVariables();
        lives.setLives(9);
        timer.setTime(0);
    }
    
    /**
     * Sets a new level state and starts the new level.
     * <p>
     * This method increments the value of level, sets the timer to 0,
     * and calls the helper method {@link #startLevel()}.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void setNextLevel() {
        level.updateLevel();
        timer.setTime(0);
        startLevel();
    }

    /**
     * Starts a new game level.
     * <p>
     * This helper method set the state of variables necessary for starting
     * or restarting a game level (e.g. whether and when a big and small meteors have
     * been spawned, the new starting location for the Ship object, and
     * the user's health level. It also removes all objects from the prior
     * level from the screen (except Ship which is repositioned).
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void startLevel() {
        bigMeteorSpawned = false;
        smallMeteorSpawned = false;
        resetShipLocation();
        removeDebris();
        removeObjects(getObjects(Button.class));
        removeObjects(getObjects(Message.class));
        health.setHealth(10);
        timer.startTimer();
        spawnTime = timer.getTime();
    }

    /**
     * Sets game difficulty variables.
     * <p>
     * Called when user chooses game difficulty ("Easy," "Hard," or "Nuts").
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void setDifficultyVariables() {
        if(difficulty.equals("easy")) {
            torpedos.setTorpedos(600);
            level2Interval = 40;
            level3Interval = 70;
            speedBig = 2;
            speedMedium = 1;
            speedSmall = 1;
            numMedium = 4;
            numSmall = 3;
        }
        if(difficulty.equals("hard")) {
            torpedos.setTorpedos(600);
            level2Interval = 35;
            level3Interval = 60;
            speedBig = 4;
            speedMedium = 2;
            speedSmall = 2;
            numMedium = 5;
            numSmall = 4;
        }
        if(difficulty.equals("nuts")) {
            torpedos.setTorpedos(800);
            level2Interval = 40;
            level3Interval = 60;
            speedBig = 6;
            speedMedium = 3;
            speedSmall = 3;
            numMedium = 8;
            numSmall = 6;
        }
    }

    /**
     * Controls creation of Big Meteor objects.
     * <p>
     * Handles creating and adding the correct number of Big Meteor objects
     * to the World for different play levels.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void createBigMeteor() {
        if(level.getLevel()==1 || level.getLevel()==2) {
            addObject(new BigMeteor(this),Greenfoot.getRandomNumber(getWidth()),80);
            bigMeteorSpawned = true;
        }
        if(level.getLevel()==3) {
            addObject(new BigMeteor(this),Greenfoot.getRandomNumber(getWidth()),80);
            addObject(new BigMeteor(this),Greenfoot.getRandomNumber(getWidth()),80);
            bigMeteorSpawned = true;
        }
    }

    /**
     * Controls creation of Medium Meteor objects.
     * <p>
     * Handles creating and adding the correct number of Medium Meteor objects
     * to the World.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void createMedMeteor(int x,int y,int num) {
        for(int r=0; r<num; r++) {
            addObject(new MediumMeteor(mediumMeteorTypes[r],this),x,y);
        }
    }

    /**
     * Controls creation of Small Meteor objects.
     * <p>
     * Handles creating and adding the correct number of Small Meteor objects
     * to the World.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void createSmallMeteor(int x,int y,int num) {
        if(!smallMeteorSpawned) {
            smallMeteorSpawned = true;
        }
        for(int r=0; r<num; r++) {
            addObject(new SmallMeteor(smallMeteorTypes[r],this),x,y);
        }
    }

    /**
     * Checks to see whether all meteors are destroyed.
     * <p>
     * This method determines whether all meteors have been been destroyed
     * and if they are it adjusts the totalTime variable and calls the 
     * appropriate progress message.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void checkMeteorsRemaining() {        
        if(getObjects(Meteor.class).size()==0 && smallMeteorSpawned && getObjects(Explosion.class).size()==0 && this.level.getLevel()==3 && this.gameOver==false) {
            timer.stopTimer();
            totalTime += getTimer().getTime();
            displayProgressMessage("youWinMess");
        }
        if(getObjects(Meteor.class).size()==0 && smallMeteorSpawned && getObjects(Explosion.class).size()==0 && level.getLevel()==2) {
            timer.stopTimer();
            totalTime += getTimer().getTime();
            displayProgressMessage("congrat2Mess");
        }
        if(getObjects(Meteor.class).size()==0 && smallMeteorSpawned && getObjects(Explosion.class).size()==0 && level.getLevel()==1) {
            timer.stopTimer();
            totalTime += getTimer().getTime();
            displayProgressMessage("congrat1Mess");
        }
    }

    /**
     * Controls the time based spawning of Big Meteor objects.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void checkBigMeteorSpawnTime() {
        if(level.getLevel()==0) {
            return;
        }
        if(level.getLevel()==2 && bigMeteorSpawned==true && timer.getTime()%level2Interval==0) {
            if(spawnTime!=timer.getTime()) { 
                bigMeteorSpawned = false;
            }
        }
        if(level.getLevel()==3 && bigMeteorSpawned==true && timer.getTime()%level3Interval==0) {
            if(spawnTime!=timer.getTime()) {
                bigMeteorSpawned = false;
            }
        }
        if(level.getLevel()==1 && bigMeteorSpawned==false || level.getLevel()==2 && bigMeteorSpawned==false || level.getLevel()==3 && bigMeteorSpawned==false ) {
            createBigMeteor();
            spawnTime = timer.getTime();
            bigMeteorSpawned = true;
        }
    }

    /**
     * Checks if there are lives and torpedos remaining.
     * <p>
     * Tracks the number of lives and torpedos remaining and ends the game
     * if either of those values reach 0.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void checkLivesAndTorpedosRemaining() {
        if(level.getLevel()==1 || level.getLevel()==2 || level.getLevel()==3) {
            if(torpedos.getTorpedos()<1 || lives.getLives()<1) {
                displayProgressMessage("youLoseMess");
            }
        }
    }

    /**
     * Checks health remaining.
     * <p>
     * If the value of heath falls below 1 this method reduces the number of
     * lives by 1, resets health to 10, and call the Try Again message.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void checkHealthRemaining() {
        if(health.getHealth()<1 && (level.getLevel()==1 || level.getLevel()==2 || level.getLevel()==3)) {
            explosionSoundLocal.play();
            lives.updateLives();
            if(lives.getLives()>0) {
                health.setHealth(10);
                Greenfoot.delay(100);
                displayProgressMessage("tryAgainMess");
            }
        }
    }

    /**
     * Removes all objects from the World.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void clearBoard() {
        removeDebris();
        removeObjects(getObjects(Meteor.class));
        removeObjects(getObjects(Message.class));
        removeObjects(getObjects(Button.class));
        removeObjects(getObjects(Ship.class));

    }

    /**
     * Removes Explosion and Torpedo objects from the World.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void removeDebris() {
        removeObjects(getObjects(Explosion.class));
        removeObjects(getObjects(Torpedo.class));
    }

    /**
     * Hides the ship and stops its movement.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void hideShip() {
        ship.setThrust(0);
        ship.setLocation(getWidth()/2,getHeight());    
    }

    /**
     * Positions the ship object at its starting location.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void resetShipLocation() {
        ship.setLocation(getWidth()/2,getHeight()-50);
        ship.setRotation(270);
    }

    /**
     * Calculates and returns the final game score value.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the calculated total score for the game
     * @since 1.0
     */
    protected int getFinalScore() {
        return score.getScore()*lives.getLives()-totalTime+torpedos.getTorpedos();
    }

    /**
     * Sets the value of the difficulty variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param difficulty the name of the difficulty level chosen by the user
     * @since 1.0
     */
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * Gets the value of the difficulty variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the difficulty variable
     * @since 1.0
     */
    protected String getDifficulty() {
        return difficulty;
    }
    
    /**
     * Gets the reference of the Timer object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Timer object
     * @since 1.0
     */
    protected Timer getTimer() {
        return timer;
    }

    /**
     * Gets the reference of the Level object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Level object
     * @since 1.0
     */
    protected Level getLevel() {
        return level;
    }

    /**
     * Gets the reference of the Torpedos object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Torpedos object
     * @since 1.0
     */
    protected Torpedos getTorpedos() {
        return torpedos;
    }

    /**
     * Gets the reference of the Score object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Score object
     * @since 1.0
     */
    protected Score getScore() {
        return score;
    }

    /**
     * Gets the reference of the Ship object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Ship object
     * @since 1.0
     */
    protected Ship getShip() {
        return ship;
    }

    /**
     * Gets the reference of the Health object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Health object
     * @since 1.0
     */
    protected Health getHealth() {
        return health;
    }

    /**
     * Gets the reference of the Lives object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the Lives object
     * @since 1.0
     */
    protected Lives getLives() {
        return lives;
    }

    /**
     * Gets the value of the TotalTime variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the TotalTime variable
     * @since 1.0
     */
    protected int getTotalTime() {
        return totalTime;
    }

    /**
     * Gets the reference of the object named getSpaceSound.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the obejct named getSpaceSound
     * @since 1.0
     */
    protected GreenfootSound getSpaceSound() {
        return spaceSound;
    }

    /**
     * Gets the value of the SpeedBig variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the SpeedBig variable
     * @since 1.0
     */
    protected int getSpeedBig() {
        return speedBig;
    }

     /**
     * Gets the value of the SpeedMedium variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the SpeedMedium variable
     * @since 1.0
     */
    protected int getSpeedMedium() {
        return speedMedium;
    }

     /**
     * Gets the value of the SpeedSmall variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the SpeedSmall variable
     * @since 1.0
     */
    protected int getSpeedSmall() {
        return speedSmall;
    }

    /**
     * Gets the reference of the GreenfootImage object named easy.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootImage object named easy
     * @since 1.0
     */
    protected GreenfootImage getEasy() {
        return easy;
    }

    /**
     * Gets the reference of the GreenfootImage object named hard.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootImage object named hard
     * @since 1.0
     */
    protected GreenfootImage getHard() {
        return hard;
    }

    /**
     * Gets the reference of the GreenfootImage object named nuts.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootImage object named nuts
     * @since 1.0
     */
    protected GreenfootImage getNuts() {
        return nuts;
    }

    /**
     * Gets the reference of the GreenfootImage object named playAgain.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootImage object named playAgain
     * @since 1.0
     */
    protected GreenfootImage getPlayAgain() {
        return playAgain;
    }

    /**
     * Gets the reference of the GreenfootImage object named rightArrow.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootImage object named rightArrow
     * @since 1.0
     */
    protected GreenfootImage getRightArrow() {
        return rightArrow;
    }

    /**
     * Gets the reference of the GreenfootImage object named leftArrow.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootImage object named leftArrow
     * @since 1.0
     */
    protected GreenfootImage getLeftArrow() {
        return leftArrow;
    }

    /**
     * Gets the reference of the GreenfootSound object named explosionSoundLocal.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return a reference to the GreenfootSound object named explosionSoundLocal
     * @since 1.0
     */
    protected GreenfootSound getExplosionSound() {
        return explosionSoundLocal;
    }
    
    /**
     * Gets the value of the numMedium variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the numMedium variable
     * @since 1.0
     */
    protected int getNumMedium() {
        return numMedium;
    }
    
    /**
     * Gets the value of the numSmall variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the numSmall variable
     * @since 1.0
     */
    protected int getNumSmall() {
        return numSmall;
    }
}
