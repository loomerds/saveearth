/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate BigMeteor objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class BigMeteor extends Meteor {
    
    private int changeRate = 4;
    private int width = 10;
    private int height = 10;
    private int speed;
    private boolean speedIsSet = false;

    /**
     * Primary constructor for objects of class BigMeteor.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param realWorld a reference to the MyWorld gameboard object
     * @since 1.0
     */
    public BigMeteor(MyWorld realWorld) {
        super(realWorld);
        try {
            getImage().scale(width,height);
        } catch (Exception e) {
            realWorld.fileMissingErrorHandler(e,true);
        }
        setSpeed();
    }

    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        controlActCount(100);
        seekRocket();
        checkCollision(1,"medium");
    }

    /**
     * Sets the value of the variable speed.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void setSpeed() {
        speed = realWorld.getSpeedBig();
    }

    /**
     * Makes the BigMeteor object hunt a Ship object and grow as it enters the gameboard.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void seekRocket() {
        if(getImage().getWidth()<80) {
            if(actCount%20==0) {
                try {
                    GreenfootImage image = new GreenfootImage("BigBoy.png");
                    width += changeRate;
                    height += changeRate;
                    image.scale(width, height);
                    setImage(image);
                } catch (Exception e) {
                    realWorld.fileMissingErrorHandler(e,true);
                }
                
                turnTowards(realWorld.getShip().getX(),realWorld.getShip().getY());
                move(speed);
            }
        } else {
            turnTowards(realWorld.getShip().getX(),realWorld.getShip().getY());
            if(actCount%5==0) {
                move(speed);
            }
        }
    }
    
    /**
     * Keeps the BigMeteor from wandering off the gameboad.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void stayOnScreen() {
        if(isTouching(MenuBar.class) || isAtEdge()) {
            seekRocket();
        }
    }
}
