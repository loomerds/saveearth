/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Timer objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Timer extends Statistic {
    
    private int time;
    private GreenfootImage timer;
    private boolean isRunning;

    /**
     * Primary constructor for objects of class Timer.
     *
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    public Timer() {
        time = 0;
        isRunning = false;
        updateTimer();
    }
    
    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        updateTimer();
        controlActCount(60);
    }
    
    /**
     * Sets the value of the time variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param time the value to which the time variable will be set
     * @since 1.0
     */
    protected void setTime(int time) {
        this.time = time;
    }
    
    /**
     * Gets the value of the time variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the time variable
     * @since 1.0
     */
    protected int getTime() {
       return time;
    }
    
    /**
     * Starts the timer.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void startTimer() {
        isRunning = true;
    }
    
    /**
     * Stops the timer.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void stopTimer() {
        isRunning = false;
    }
    
    /**
     * Adds 1 to the value of the time variable and updates the Timer statistics box. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void updateTimer() {
        if(isRunning==true && actCount%60==0) {
            time++;
            makeStatisticBox("Time",""+time);
        }
    }
}
