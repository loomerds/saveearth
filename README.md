# Save Earth!

***Save Earth!*** is a modest *Asteroids* inspired game developed using Greenfoot - a simple IDE/Framework for game and simulation programming in the Java programming language. It was created to provide Introduction to Programming with Java students with an exemplar for basic Object Oriented Programming design and development.

![Save Earth! in action](/images/action.png)


To play this game:

- git pull the program source files to your local computer from this repository

- download and install [Greenfoot](https://www.greenfoot.org/download) (it is free)

- launch Greenfoot, click on the *Scenario* tab, click on *Open...*, and select the folder you git pulled from this site.

