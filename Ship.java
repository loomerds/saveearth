/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Ship objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Ship extends TimedActor {

    private int thrust = 2;
    private int maxThrust = 5;
    private int minThrust = -5;
    private int thrustTime = 1;
    private int turnHardLeft = 0;
    private int turnHardRight = 0;
    private int hardLeftRequestThreshhold = 5;
    private int hardRightRequestThreshhold = 5;
    private boolean spaceKeyPressed = false;
    private boolean rightKeyPressed = false;
    private boolean leftKeyPressed = false;
    private boolean realWorldIsSet = false;
    private int leftTurnDegrees = 0;
    private int rightTurnDegrees = 0;
    private static final GreenfootImage rocket;
    private static final GreenfootImage rocketNoThrust;
    private static final GreenfootImage rocketBackThrust;
    private static final GreenfootImage rocketLeftThrust;
    private static final GreenfootImage rocketRightThrust;
    private static final GreenfootSound thrustSound;

    static {
        rocket = new GreenfootImage("rocket.png");
        rocketNoThrust = new GreenfootImage("rocketNoThrust.png");
        rocketBackThrust = new GreenfootImage("rocketBackThrust.png");
        rocketLeftThrust = new GreenfootImage("rocketLeftThrust.png");
        rocketRightThrust = new GreenfootImage("rocketRightThrust.png");
        thrustSound = new GreenfootSound("sounds/thrust.wav");
    }

    /**
     * Primary constructor for objects of class Ship.
     * <p>
     * The Ship object is the user controlled object for the game.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    public Ship() throws ExceptionInInitializerError {
        setRocketImages();
        setRotation(270);
    }

    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        controlActCount(100);
        fireTorpedo();
        resetTurnDegrees();
        resetHardTurnRequests();
        fireThrusters();
        moveRocket();
        stayOnScreen();
        keyToggle();
    }
    
    /**
     * Sets the value of the inherited realWorld reference variable to an 
     * object of type MyWorld. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void setRealWorld() {
        if(realWorldIsSet==false) {
            super.setRealWorld();
            realWorldIsSet = true;
        }
    }
    
    /**
     * Sets and scales the five GreenfootImages used by Ship. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void setRocketImages() {
        setImage(rocketRightThrust);
        getImage().scale(35,25);
        setImage(rocket);
        getImage().scale(35,25);
        setImage(rocketBackThrust);
        getImage().scale(35,25);
        setImage(rocketLeftThrust);
        getImage().scale(35,25);
        setImage(rocketNoThrust);
        getImage().scale(35,25);
    }

    /**
     * Controls the speed of the Ship object. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void moveRocket() {
        if(actCount%2==0) {
            move(thrust);
        }
    }

    /**
     * Provides user with keyboard interactive Ship control.
     * <p>
     * Note that each keyboard movement command also involves changes to the Ship object's
     * appearance and plays the Ship's thruster sound.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void fireThrusters() {
        if(Greenfoot.isKeyDown("up")) {
            thrustSound.stop();
            thrustSound.play();
            if(thrust<=maxThrust) {
                thrust = thrust+1;
            }
            if(!(getImage()==rocket)) {
                setImage(rocket);
            }
            return;
        } else if(Greenfoot.isKeyDown("down")) {
            thrustSound.stop();
            thrustSound.play();
            if(thrust>=minThrust) {
                thrust = thrust-1;
            }
            if(!(getImage()==rocketBackThrust)) {
                setImage(rocketBackThrust);
            }
            return;
        } else if(Greenfoot.isKeyDown("right")) {
            thrustSound.stop();
            thrustSound.play();
            turnHardRight++;
            if(thrust!=0 && turnHardRight<=hardRightRequestThreshhold) {
                setImage(rocketRightThrust);
                turn(2);
            }
            if(turnHardRight>hardRightRequestThreshhold) {
                stopRocket();
            }
            if(thrust==0) {
                if(actCount%4 == 0) {
                    if(rightTurnDegrees<12) {
                        rightTurnDegrees += 2;
                    }
                }
                turn(rightTurnDegrees);
                if(!(getImage()==rocketRightThrust)) {
                    setImage(rocketRightThrust);
                }
            }
            return;
        } else if(Greenfoot.isKeyDown("left")) {           
            thrustSound.stop();
            thrustSound.play();
            turnHardLeft++;
            if(thrust!=0 && turnHardLeft<=hardLeftRequestThreshhold) {
                setImage(rocketLeftThrust);
                turn(-2);
            }
            if(turnHardLeft>hardLeftRequestThreshhold) {
                stopRocket();
            }
            if(thrust==0) {
                if(actCount%4==0) {
                    if(leftTurnDegrees>-12) {
                        leftTurnDegrees += -2;
                    }
                }
                turn(leftTurnDegrees);
                if(!(getImage()==rocketLeftThrust)) {
                    setImage(rocketLeftThrust);
                }
            }
            return;
        } else {
            if(!(getImage()==rocketNoThrust)) {
                setImage(rocketNoThrust);
            }
        }
    }

    /**
     * Slows the Ship's speed to full stop.
     * <p>
     * Note a call to this method also involves changes to the state of the Ship object's
     * appearance. This is a helper method for the {@link #fireThrusters()} method.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void stopRocket() {
        if(thrust < 0) {
            thrust = thrust+1;
            if(thrust>0) {
                thrust = 0;
            }
            if(!(getImage()==rocket)) {
                setImage(rocket);
            }
        }
        if(thrust > 0) {
            thrust = thrust-1;
            if(thrust<0) {
                thrust = 0;
            }
            if(!(getImage()==rocketBackThrust)) {
                setImage(rocketBackThrust);
            }
        }
    }

    /**
     * Resets the turnDegrees variable of the Ship.
     * <p>
     * This is a helper method for the {@link #fireThrusters()} method.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void resetTurnDegrees() {
        if(!(Greenfoot.isKeyDown("left"))) {
            leftTurnDegrees = -2;
        }
        if(!(Greenfoot.isKeyDown("right"))) {
            rightTurnDegrees = 2;
        }
    }

    /**
     * Resets the the hardTurnRequests variable of the Ship to 0.
     * <p>
     * This is a helper method for the {@link #fireThrusters()} method.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void resetHardTurnRequests() {
        if(!(Greenfoot.isKeyDown("left"))) {
            turnHardLeft = 0;
        }
        if(!(Greenfoot.isKeyDown("right"))) {
            turnHardRight = 0;
        }
    } 

    /**
     * Controls the intialization and display of Torpedo objects.
     * <p>
     * This method syncronizes the vector of a Torpedo object fired with that of the Ship
     * object at the moment of firing. The method also updates the state of the Torpedos
     * statistics box.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void fireTorpedo() {
        if(actCount%8==0) {
            if(Greenfoot.isKeyDown("space") && spaceKeyPressed==false) {
                int turnDegrees = 0;
                if(Greenfoot.isKeyDown("left")==true) {
                    turnDegrees = leftTurnDegrees;
                } 
                if(Greenfoot.isKeyDown("right")==true) {
                    turnDegrees = rightTurnDegrees;
                }
                try {
                    realWorld.addObject(new Torpedo(getRotation()+turnDegrees*2,this.thrust),getX(),getY());
                } catch (Throwable e) {
                    realWorld.fileMissingErrorHandler(e,true);
                }
                realWorld.getTorpedos().updateTorpedos();
                spaceKeyPressed = true;
            }
        }
    }

    /**
     * Keeps the Ship object from leaving the gameboad.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void stayOnScreen() {
        if(isAtEdge()) {
            if(thrust>-1) {
                turn(Greenfoot.getRandomNumber(40)+120);
                move(10);
            } else {
                thrust *= -1;
                move(10);
            }
        }
    }

    /**
     * Ensures that pressing the space key only triggers a single event.
     * <p>
     * This method stops the user from firing multiple Torpedos by simply holding down
     * the space key.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void keyToggle() {
        if(!(Greenfoot.isKeyDown("space"))) {
            spaceKeyPressed = false;
        }
        if(!(Greenfoot.isKeyDown("right"))) {
            rightKeyPressed = false;
        }
        if(!(Greenfoot.isKeyDown("left"))) {
            leftKeyPressed = false;
        }
    }

    /**
     * Sets the value of the thrust variable to the value passed.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param thrust the value to which the Ship object's thrust variable should be set
     * @since 1.0
     */
    protected void setThrust(int thrust) {
        this.thrust = thrust;
    }
    
    /**
     * Gets the value of the Ship's thrust variable.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @return the value of the Ship's thrust variable
     * @since 1.0
     */
    protected int getThrust() {
        return thrust;
    }
}
