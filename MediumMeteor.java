/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate MediumMeteor objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class MediumMeteor extends Meteor {
    
    private int changeRate = 4;
    private int width = 50;
    private int height = 50;
    private int speed = 1;
    private int range;
    private int offset;
    private boolean firstMove = false;
    
    /**
     * Primary constructor for objects of class MediumMeteor.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param shape a reference to the GreenfootImage file that the object will use
     * @param realworld a reference to the MyWorld gameboard object
     * @since 1.0
     */
    public MediumMeteor(GreenfootImage shape,MyWorld realWorld) {
        super(realWorld);
        try {
            setImage(shape);
            getImage().scale(width,height);
            range = 80;
            offset = 140;

        } catch (Exception e) {
            realWorld.fileMissingErrorHandler(e,true);
        }
    }
    
    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        controlActCount(100);
        setRealWorld();
        stayOnScreen();
        checkCollision(1,"small");
        drift();
    }
    
    /**
     * Sets the value of the variable speed.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void setSpeed() {
        speed = realWorld.getSpeedMedium();
    }
    
    /**
     * Makes the MediumMeteor drift in a random direction after being instantiated.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void drift() {
        if(firstMove==false) {
            turn(Greenfoot.getRandomNumber(359));
            move(30);
            firstMove = true;
        }
        if(actCount%5 == 0) {
            move(speed);
        }
    }
    
    /**
     * Keeps the MediumMeteor from wandering off the gameboad.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void stayOnScreen() {
        if(isTouching(MenuBar.class) || isAtEdge()) {
            turnTowards(getWorld().getWidth()/2-((int)(Math.random())*200-100),getWorld().getHeight()/2-((int)(Math.random())*200-100));
        }
    }
}
