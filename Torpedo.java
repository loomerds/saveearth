/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Torpedo objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Torpedo extends TimedActor {

    private boolean isFired = false;
    private boolean torpedoSoundIsSet = false;
    private boolean realWorldIsSet = false;
    private int shipThrust;
    private int torpedoSpeed = 10;
    private GreenfootSound photonSound;
    private static final GreenfootImage photonImageClear;
    private static final GreenfootImage photonImage;

    static {
            photonImageClear = new GreenfootImage("torpedoClear.png");
            photonImage = new GreenfootImage("torpedo.png");
    }

    /**
     * Primary constructor for objects of class Torpedo.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param myRotation the vector for the Torpedo
     * @param shipThrust the speed at which the Ship firing the Torpedo is moving
     * @since 1.0
     */
    public Torpedo(int myRotation,int shipThrust) throws ExceptionInInitializerError {
        setImage(photonImage);
        getImage().scale(10,5);
        setRotation(myRotation);
        this.shipThrust = shipThrust;
        setImage(photonImageClear);
        getImage().scale(1,1);
    }

    public Torpedo() {}
    
    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() {
        setRealWorld();
        setTorpedoSound();
        fireTorpedo(shipThrust);
        offScreen();
        controlActCount(100);
    }
    
    /**
     * Sets the value of the inherited realWorld reference variable to an 
     * object of type MyWorld. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void setRealWorld() {
        if(realWorldIsSet==false) {
            super.setRealWorld();
            realWorldIsSet = true;
        }
    }
    
    /**
     * Gives a Torpedo object its own GreenfootSound object.
     * <p>
     * It was decided when balancing performance speed against game enjoyment
     * quality that each Torpedo object should make its own noise rather than
     * access a shared static final sound object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void setTorpedoSound() {
        if(torpedoSoundIsSet==false) {
            try {
                photonSound = new GreenfootSound("sounds/photonTorpedo.wav");
                torpedoSoundIsSet = true;
            } catch (Throwable e) {
                realWorld.fileMissingErrorHandler(e,true);
            }
        }
    }

    /**
     * Manages the speed, appearance, and sound made by a Torpedo object.
     * <p>
     * Each Torpedo object may only play its sound file one time.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void fireTorpedo(int shipThrust) {
        if(actCount>1) {
            if(shipThrust>0){
                move(torpedoSpeed+shipThrust);
            } else {
                move(torpedoSpeed);
            }
        } else {
            if(getImage()!=photonImage) {
                setImage(photonImage);
            }
            if(shipThrust>0){
                move(torpedoSpeed+shipThrust);
            }
            if(shipThrust==0) {
                move(torpedoSpeed);
                setImage(photonImage);
            }
            if(shipThrust<0) {
                move(torpedoSpeed+shipThrust);
                setImage(photonImage);
            }
            actCount++;
            if(isFired==false) {
                try {
                    photonSound.play();
                } catch(Throwable e) {
                    realWorld.fileMissingErrorHandler(e,false);
                }
                isFired = true;
            }
        }
    }

    /**
     * Removes the Torpedo object from the World if it leaves the gameboad.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void offScreen() {
        if(isAtEdge()) {
            getWorld().removeObject(this);
        }
    }
}
