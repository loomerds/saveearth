/**  
 *  Copyright (C) 2019 Doug Loomer
 *  Email: doug@dougLoomer.com
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *  
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *
 *  A copy of the GNU General Public License is included in the source code for
 *  this program within the file named License.txt. You may also obtain a copy 
 *  of the license by writing to the Free Software Foundation, Inc., 59
 *  Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 *  This file is subject to the Classpath exception as provided in the
 *  License.txt file.
 */

import greenfoot.*;

/**
 * This class is used to instantiate Explosion objects.
 * 
 * @author (Doug Loomer) 
 * @version (1.0)
 * @since (1.0)
 */
public class Explosion extends TimedActor {

    private boolean hasExploded = false;
    private boolean realWorldIsSet = false;
    private boolean explosionSoundIsSet = false;
    private GreenfootSound explosionSound;

    public Explosion() {}

    /**
     * Primary constructor for objects of class Explosion.
     * <p>
     * Explosions come in two sizes - large and small.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @param size a value of 0 make small sized explosions, other values make
     * large sized explosions
     * @since 1.0
     */
    public Explosion(int size) {        
        if(size==0) {
            getImage().scale(20,20);
        } else {
            getImage().scale(60,60);
        }
    }

    /**
     * Method called by Greenfoot at each execution cycle.
     * <p>
     * The code in act() determines how an object behaves given its
     * present state.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    public void act() 
    {
        setRealWorld();
        setExplosionSound();        
        makeNoise();
        removeExplosion();
        controlActCount(100);
    }

    /**
     * Sets the value of the inherited realWorld reference variable to an 
     * object of type MyWorld. 
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    @Override
    protected void setRealWorld() {
        if(realWorldIsSet==false) {
            super.setRealWorld();
            realWorldIsSet = true;
        }
    }

    /**
     * Gives an Explosion object its own GreenfootSound object.
     * <p>
     * It was decided when balancing performance speed against game enjoyment
     * quality that each Explosion object should make its own noise rather than
     * access a shared static final sound object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    private void setExplosionSound() {
        if(explosionSoundIsSet==false) {    
            try {
                explosionSound = new GreenfootSound("sounds/explosion.wav");
                explosionSoundIsSet = true;
            } catch (Throwable e) {
                realWorld.fileMissingErrorHandler(e,true);
            }
        }
    }

    /**
     * Plays the Explosion sound file once.
     * <p>
     * Each Explosion object may only play its sound file one time.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void makeNoise() {
        if(hasExploded==false) {
            try{
                explosionSound.play();
            } catch (Throwable e) {
                realWorld.fileMissingErrorHandler(e,true);
            }
            hasExploded = true;
        }
    }

    /**
     * Removes the Explosion object after a fixed time.
     * <p>
     * The method uses the inherited actCount variable and the inherited method
     * {@link TimedActor#controlActCount(int actCountMax)} to time the removal
     * of the Explosion object.
     * 
     * @author Doug Loomer
     * @version 1.0
     * @since 1.0
     */
    protected void removeExplosion() {
        if(actCount%50==0) {
            realWorld.removeObject(this);
        }
    }
}
